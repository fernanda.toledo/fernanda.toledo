### Hi! I'm Fernanda 👩🏻

 I work at [GitLab](https://about.gitlab.com/) as a Frontend Engineer on the [Plan:Project Management](https://handbook.gitlab.com/handbook/engineering/development/dev/plan/project-management/) team.

I'm committed to delivering pixel-perfect websites that reflect the company goals, design vision and user needs. ✨

📍Vancouver, Canada
